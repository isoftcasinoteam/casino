/*
 * @author Emil Yordanov
 * 06-07-2019
 */

package data;

public class User {
    private String firstName;

    private String secondName;

    private String thirdName;

    private String nickname;

    private Integer age;

    private String email;

    private String passportID;

    private String password;

    private boolean isBlocked;

    public User(
            String firstName,
            String secondName,
            String thirdName,
            String nickname,
            Integer age,
            String email,
            String passportID,
            String password) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.thirdName = thirdName;
        this.nickname = nickname;
        this.age = age;
        this.email = email;
        this.passportID = passportID;
        this.password = password;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return {@link #firstName}
     */
    public String getFirstName() {
        return firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * @return {@link #secondName}
     */
    public String getSecondName() {
        return secondName;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    /**
     * @return {@link #thirdName}
     */
    public String getThirdName() {
        return thirdName;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * @return {@link #nickname}
     */
    public String getNickname() {
        return nickname;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return {@link #age}
     */
    public Integer getAge() {
        return age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return {@link #email}
     */
    public String getEmail() {
        return email;
    }

    public void setPassportID(String passportID) {
        this.passportID = passportID;
    }

    /**
     * @return {@link #passportID}
     */
    public String getPassportID() {
        return passportID;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return {@link #password}
     */
    public String getPassword() {
        return password;
    }

    public void setIsBlocked(boolean blocked)
        {
        isBlocked = blocked;
        }

    public boolean getIsBlocked()
        {
        return isBlocked;
        }
}
