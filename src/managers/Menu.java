/*
 * @author Emil Yordanov
 * 06-07-2019
 */

package managers;

import java.util.HashMap;
import java.util.Scanner;

import constants.Files;
import org.json.JSONArray;
import org.json.JSONObject;
import services.serializator.JSONSerializatorService;
import services.user.UserService;
import utilities.Validator;


public class Menu {
    private static JSONSerializatorService jsonSerializatorService = new JSONSerializatorService();

    private static UserService userService = new UserService();

    private static Scanner keyboard = new Scanner(System.in);

    private Menu() {

    }

    public static void showRegisterForm() {
        /**
         * Creating a json object of the content of the file and getting the sub menu
         * of the register menu.
         */
        JSONObject outer = Menu.jsonSerializatorService.createObjectFromFileContent(Files.MENUS);
        JSONObject registerMap = outer.getJSONObject("registerMenu");
        JSONArray subElementsOfRegister = registerMap.getJSONArray("sub");

        HashMap<String, String> userRegisterInfo = Menu.gettingUserRegisterInfo(subElementsOfRegister);

        if(!Validator.isUserInputCorrect(userRegisterInfo)) {
            int age = Integer.parseInt(userRegisterInfo.get("age"));
            Menu.userService.registerUser(
                    userRegisterInfo.get("firstName"),
                    userRegisterInfo.get("secondName"),
                    userRegisterInfo.get("thirdName"),
                    userRegisterInfo.get("nickname"),
                    age,
                    userRegisterInfo.get("email"),
                    userRegisterInfo.get("passportID"),
                    userRegisterInfo.get("password"));
        }
    }

    public static void showLoginForm(){
        JSONObject outer = Menu.jsonSerializatorService.createObjectFromFileContent(Files.MENUS);
        JSONObject registerMap = outer.getJSONObject("loginMenu");
        JSONArray subElementsOfRegister = registerMap.getJSONArray("sub");

        HashMap<String, String> userLoginInfo = Menu.gettingUserRegisterInfo(subElementsOfRegister);

        Menu.userService.loginUser(userLoginInfo.get("nickname"), userLoginInfo.get("password"));
    }

    /**
     *
     * @param subElementsOfRegister the sub elements of the register menu in our menus.json file
     * @return HashMap with the user input on the registration process
     */
    private static HashMap<String, String> gettingUserRegisterInfo(JSONArray subElementsOfRegister) {
        /**
         * Creating the map to keep for key the current input(firstName, secondName,...)
         * and for value the entered value from the user.
         */
        HashMap<String, String> userRegisterInfo = new HashMap<>();

        /**
         * Traversing through the elements of the sub menu.
         */
        for (int i = 0; i < subElementsOfRegister.length(); i++) {
            /**
             * Getting the current json object and after i have it i can show to the user
             * the prompt message. In our json file we have a key string,which keeps
             * value for the specific input the user is on.If he is on entering his
             * first menu we have a value firstName from the json file,on entering
             * second name it is the second name and etc.
             */
            JSONObject currentObject = subElementsOfRegister.getJSONObject(i);

            String promptMessage = currentObject.getString("promptMessage");
            System.out.print(promptMessage);

            String keyUserEntry = currentObject.getString("key");
            String valueUserEntry = keyboard.nextLine();

            userRegisterInfo.put(keyUserEntry, valueUserEntry);
        }

        return userRegisterInfo;
    }
}
