package utilities;

import app.CasinoManager;
import data.User;

import java.util.HashMap;

/**
 * Validates the user input for the registration process.
 */

public class Validator {
    /**
     * Checks if the given name is in correct format(firstName, secondName, thirdName, nickname)
     * @param name
     * @return
     */
    public static boolean isFullNamePartsCorrect(String name) {
        String pattern = "^[a-zA-z]{3,}$";
        if(name.matches(pattern))
            return true;
        return false;
    }

    public static boolean isNicknameCorrect(String nickname) {
        String pattern = "^[\\w]+$";
        if(nickname.matches(pattern))
            return true;
        return false;
    }

    /**
     * Checks if the given username exists in the json file.
     * @param username
     * @return
     */
    public static boolean isUniqueUsername(String username){
        for (User user : CasinoManager.users) {
            if(user.getNickname().equals(username))
                return false;
        }

        return true;
    }

    /**
     * Check if the age is integer.If it is not i catch exception and if it is integer
     * i parse it to check it with the method for valid age.
     *
     * @param age the age of the user
     * @return
     */
    public static boolean isAgeCorrectNumber(String age) {
        try {
            int parsedAge = Integer.parseInt(age);
            if (!Validator.isValidAge(parsedAge)) {
                System.out.println("Your age should be between 18 and 120.");
                return false;
            }
        } catch (NumberFormatException e) {
            System.out.println("Impossible age");
            return false;
        }

        return true;
    }

    /**
     * Checks if age is between 18 and 120
     * @param age
     * @return
     */
    public static boolean isValidAge(int age){
        if(age < 18 || age > 120)
            return false;
        return true;
    }

    /**
     * Checks if the given email is valid based on regex.
     * @param email
     * @return
     */
    public static boolean isValidEmail(String email){
        String pattern = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
        if(email.matches(pattern))
            return true;
        return false;
    }

    /**
     * Checks if the email is free
     * @param email
     * @return
     */
    public static boolean isUniqueEmail(String email) {
        for (User user : CasinoManager.users) {
            if(user.getEmail().equals(email)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if passport ID is in correct format.
     * @param passportID
     * @return
     */
    public static boolean isValidPassportID(String passportID) {
        String pattern = "^[a-zA-z0-9]{3,}$";
        if(passportID.matches(pattern))
            return true;
        return false;
    }

    /**
     * Checks if the given password is valid.
     * @param password
     * @return true if length is more than 4.
     */
    public static boolean isValidPassword(String password){
        if(password.length() < 4)
            return false;
        return true;
    }

    /**
     * Checks if the two given passwords match.
     * @param password
     * @param repeatedPassword
     * @return
     */
    public static boolean passwordsMatch(String password, String repeatedPassword){
        if(password.equals(repeatedPassword))
            return true;
        return false;
    }

    public static boolean isUserInputCorrect(HashMap<String, String> userRegisterInfo) {
        if(
                userRegisterInfo.get("firstName").equals("") ||
                userRegisterInfo.get("secondName").equals("") ||
                userRegisterInfo.get("thirdName").equals("") ||
                userRegisterInfo.get("nickname").equals("") ||
                userRegisterInfo.get("age").equals("") ||
                userRegisterInfo.get("email").equals("") ||
                userRegisterInfo.get("passportID").equals("") ||
                userRegisterInfo.get("password").equals("") ||
                userRegisterInfo.get("repeatedPassword").equals("")) {
            System.out.println("Some of your fields are empty." );
            return true;
        } else if(!Validator.isFullNamePartsCorrect(userRegisterInfo.get("firstName"))) {
            System.out.println("Not correct first name.");
            return true;
        } else if(!Validator.isFullNamePartsCorrect(userRegisterInfo.get("secondName"))) {
            System.out.println("Not correct second name.");
            return true;
        } else if(!Validator.isFullNamePartsCorrect(userRegisterInfo.get("thirdName"))) {
            System.out.println("Not correct third name.");
            return true;
        } else if(!Validator.isNicknameCorrect(userRegisterInfo.get("nickname"))) {
            System.out.println("Not correct nickname.");
            return true;
        } else if(!Validator.isUniqueUsername(userRegisterInfo.get("nickname"))) {
            System.out.println("The nickname is already taken.");
            return true;
        } else if(!Validator.isAgeCorrectNumber(userRegisterInfo.get("age"))) {
            return true;
        } else if(!Validator.isValidEmail(userRegisterInfo.get("email"))) {
            System.out.println("Not correct email.");
            return true;
        } else if(!Validator.isUniqueEmail(userRegisterInfo.get("email"))) {
            System.out.println("The email is already taken.");
            return true;
        } else if(!Validator.isValidPassportID(userRegisterInfo.get("passportID"))) {
            System.out.println("Not correct passportID.");
            return true;
        } else if(!Validator.isValidPassword(userRegisterInfo.get("password")) || !Validator.isValidPassword(userRegisterInfo.get("repeatedPassword"))) {
            System.out.println("Not correct password.");
            return true;
        } else if(!Validator.passwordsMatch(userRegisterInfo.get("password"), userRegisterInfo.get("repeatedPassword"))) {
            System.out.println("Passwords mismatch.");
            return true;
        }

        return false;
    }
}
