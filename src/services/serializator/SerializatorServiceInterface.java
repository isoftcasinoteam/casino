package services.serializator;

public interface SerializatorServiceInterface {
    /**
     * Method for showing the main menu for our user. It accepts only the file
     * name and it print the menu.The file can be json,xmx or csv. That way i demand
     * the class who uses my interface to implement his own logic for that method.
     *
     * @param fileName the name of the desired file
     */
    void showMainMenu(String fileName);
}
