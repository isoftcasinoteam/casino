package services.serializator;

import data.User;

import java.util.List;

public abstract class Serializator {
    /**
     * Getting all users from our file on the start of the program. At first it will be
     * only admin and returning array list with one element.
     *
     * @param fileName the name of the file
     * @return getting all users as objects in an array list
     */
    protected abstract List<User> getAllUsersAsArray(String fileName);

    /**
     * Method for creating object from file content.It accepts only the file
     * name and it cast the file content to different type of object.The
     * casted object from the file content can be json,xml or csv and that
     * is why i make it generic method.That way i demand the class who uses
     * my interface to implement his own logic for that method.
     *
     * @param fileName the name of the file
     * @param <T> the param can be different type of file:xml,json or csv
     * @return
     */
    protected abstract <T> T createObjectFromFileContent(String fileName);
}