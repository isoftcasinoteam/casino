/*
 * @author Emil Yordanov
 * 06-07-2019
 */

package services.serializator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import app.CasinoManager;
import com.google.gson.Gson;
import data.User;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.logging.Level;
import java.util.logging.Logger;

public class JSONSerializatorService extends Serializator implements SerializatorServiceInterface {
    private Logger logger = Logger.getGlobal();

    public JSONSerializatorService() {
    }

    /**
     * Printing the main menu,his welcome message and his inner elements.
     *
     * @param fileName the file name which we want to show the main menu from
     */
    @Override
    public void showMainMenu(String fileName) {
        JSONObject fileAsJsonObject = createObjectFromFileContent(fileName);
        String mainMenu = "mainMenu";
        printMenuWelcomeMessage(fileAsJsonObject, mainMenu);
        printInnerElementsOfMenu(fileAsJsonObject, mainMenu);
    }

    /**
     * Getting all users as objects in array list
     *
     * @param fileName the file name which we must read to get our stored users
     * @return the array list of user objects
     */
    @Override
    public List<User> getAllUsersAsArray(String fileName) {
        Gson gson = new Gson();

        /*
        Getting the content of the file.It gets the file content like a wrapper.
         */
        JSONSerializatorService jsonSerializatorService = new JSONSerializatorService();
        JSONObject outer = jsonSerializatorService.createObjectFromFileContent(fileName);

        if(outer != null)  {
            /**
             * Iterator to get the keys and iterate through them.
             */
            Iterator<String> keys = outer.keys();

            while(keys.hasNext()) {
                /**
                 * Getting the current json object against a key and cast it to user
                 * object. After that we put the user object in our list.
                 */
                String key = keys.next();
                String currentUserAsString = outer.getJSONObject(key).toString();
                User user = gson.fromJson(currentUserAsString, User.class);
                CasinoManager.users.add(user);
            }
        }

        return CasinoManager.users;
    }

    /**
     * Getting the content of the file
     *
     * @param fileName the file name which we use for getting the content of
     * @return the content of the desired file
     */
    @Override
    public JSONObject createObjectFromFileContent(String fileName) {
        JSONObject outer = null;

        try {
            String content = new String(Files.readAllBytes(Paths.get(fileName)));
            outer = new JSONObject(content);
        } catch (IOException e){
            if(fileName.equals(constants.Files.MENUS)) {
                System.err.println("Sorry but our menu service is temporarily unavailable. Please come back later.");
                logger.log(Level.SEVERE, "Our file for the menus is missing or someone is using it now.", e);
            } else if(fileName.equals(constants.Files.USERS)) {
                System.err.println("Sorry but our user service is temporarily unavailable. Please come back later.");
                logger.log(Level.SEVERE, "Our file for the menus is missing or someone is using it now.", e);
            }

        } catch (JSONException jsonExc) {
            if(fileName.equals(constants.Files.MENUS)) {
                System.err.println("Sorry but our menu service is temporarily unavailable. Please come back later.");
                logger.log(Level.SEVERE, "Our file for the users is in wrong format.", jsonExc);
            } else if(fileName.equals(constants.Files.USERS)) {
                System.err.println("Sorry but our user service is temporarily unavailable. Please come back later.");
                logger.log(Level.SEVERE, "Our file for the users is in wrong in format.", jsonExc);
            }
        }

        return outer;
    }

    /**
     * Printing the welcome message for the desired menu from our menus.json.
     *
     * @param outer the content of the desired file
     * @param typeOfMenu the type of menu the user must see
     */
    private void printMenuWelcomeMessage(JSONObject outer, String typeOfMenu) {
        try {
            JSONObject mainMenuJSONObject = outer.getJSONObject(typeOfMenu);
            String welcomeMessage = mainMenuJSONObject.getString("message");

            System.out.println(welcomeMessage);
        } catch (JSONException jsonExc) {
            logger.log(Level.SEVERE, "The file for menus is in wrong json format", jsonExc);
        }
    }

    /**
     * Printing the inner elements of the desired menu.
     *
     * @param outer the content of the desired file
     * @param typeOfMenu the type of menu the user must see
     */
    private void printInnerElementsOfMenu(JSONObject outer, String typeOfMenu) {
        try {
            JSONObject mainMenuJSONObject = outer.getJSONObject(typeOfMenu);
            JSONArray mainMenuJSON = mainMenuJSONObject.getJSONArray("sub");

            for (int i = 0; i < mainMenuJSON.length(); i++) {
                JSONObject currentObject = mainMenuJSON.getJSONObject(i);
                String menuEntry = currentObject.getString("promptMessage");
                System.out.println(menuEntry);
            }
        } catch (JSONException jsonExc) {
            logger.log(Level.SEVERE, "The file for menus is in wrong json format", jsonExc);
        }
    }
}
