/*
 * @author Emil Yordanov
 * 06-07-2019
 */

package services.user;

import app.CasinoManager;
import constants.Files;
import data.User;
import org.json.JSONException;
import org.json.JSONObject;
import services.encryption.BCryptEncriptionService;
import services.serializator.JSONSerializatorService;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import utilities.Validator;

public class UserService implements UserServiceInterface {
    private BCryptEncriptionService bCrypt;

    private JSONSerializatorService jsonSerializatorService;

    private Logger logger = Logger.getGlobal();

    public UserService() {
        bCrypt = new BCryptEncriptionService();
        jsonSerializatorService = new JSONSerializatorService();
    }

    @Override
    public void registerUser(String firstName,
                             String secondName,
                             String thirdName,
                             String nickname,
                             Integer age,
                             String email,
                             String passportID,
                             String password) {
        /*
        Hashing the received password
         */
        String computed_hash = bCrypt.hashPassword(password);

        /*
        We create user by the received data
         */
        User user = new User(firstName, secondName, thirdName, nickname, age, email, passportID, computed_hash);
        Gson gson = new Gson();

        JSONObject outer = jsonSerializatorService.createObjectFromFileContent(Files.USERS);

        if(outer != null) {
            String userAsJson = gson.toJson(user);
            JSONObject userAsJSONObject = new JSONObject(userAsJson);

            outer.put(nickname, userAsJSONObject);

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(Files.USERS))) {
                writer.write(outer.toString(2));
            } catch (IOException e) {
                System.err.println("Sorry but our register process is temporarily unavailable. Please come back later.");
                logger.log(Level.SEVERE, "Our file for the users is missing or someone is using it now.", e);
            }

            CasinoManager.users.add(user);

            System.out.println("Welcome to our casino.Time to earn some money.");
        }
    }



    public void loginUser(String nickname, String password)
        {

        int loginAttempts = 0;

        if (nickname.equals("") || password.equals(""))
            System.out.println("You didn't fill the required fields");
        else if (Validator.isUniqueUsername(nickname))
            System.out.println("Wrong credentials");

        else
            {
            User foundedUser = getUserByNickname(nickname);
            String hashedPassword = foundedUser.getPassword();
                if (bCrypt.checkPassword(password, hashedPassword)) {
                    System.out.println("Login successful");
                } else {
                    loginAttempts++;
                    System.out.println(loginAttempts);
                    if(loginAttempts == 3) {
                        foundedUser.setIsBlocked(true);
                    }
                    System.out.println(foundedUser.getIsBlocked());
                }
            }
        }

    @Override
    public User getUserByNickname(String nickname)
        {
        User user = null;
        for (User currentUser : CasinoManager.users) {
        if(currentUser.getNickname().equals(nickname))
            user = currentUser;
        }
        return user;
        }
}
