/*
 * @author Emil Yordanov
 * 06-07-2019
 */

package services.user;

import data.User;

public interface UserServiceInterface {
    void registerUser(
            String firstName,
            String secondName,
            String thirdName,
            String nickname,
            Integer age,
            String email,
            String passportID,
            String password);

    User getUserByNickname(String nickname);
}
