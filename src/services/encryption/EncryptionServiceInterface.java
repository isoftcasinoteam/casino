/*
 * @author Emil Yordanov
 * 06-07-2019
 */

package services.encryption;

public interface EncryptionServiceInterface {
    String hashPassword(String password);

    boolean checkPassword(String passwordHash, String passwordString);
}
