package constants;

public final class Files {
    private Files() {}

    public static final String USERS = "src/database/users.json";
    public static final String MENUS = "src/menusFile/menus.json";
}
