/*
 * @author Emil Yordanov
 * 05-07-2019
 */

package app;

import constants.Files;
import data.User;
import managers.Menu;
import services.serializator.JSONSerializatorService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CasinoManager {
    private JSONSerializatorService jsonSerializatorService;

    private Scanner keyboard;

    public static List<User> users = new ArrayList<>();

    public CasinoManager() {
        keyboard = new Scanner(System.in);
        jsonSerializatorService = new JSONSerializatorService();
        users = jsonSerializatorService.getAllUsersAsArray(Files.USERS);
    }

    public void run() {
        jsonSerializatorService.showMainMenu(Files.MENUS);

        String choice;
        boolean flag = true;

        while (flag) {
            System.out.print("Enter your choice[1-3]:");

            if (keyboard.hasNextLine()) {
                choice = keyboard.nextLine();

                switch (choice) {
                    case "1":
                        Menu.showLoginForm();
                        break;
                    case "2":
                        Menu.showRegisterForm();
                        break;
                    case "3":
                        System.out.println("Out we go.");
                        flag = false;
                        break;
                    default:
                        System.out.println("Try again");
                        break;
                }
            }
        }
    }
}
