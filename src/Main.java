/*
 * @author Emil Yordanov
 * 05-07-2019
 */

import app.CasinoManager;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
	public static void main(String[] args) {
		Logger logger = Logger.getGlobal();

		String logTime = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(new Date());
		String logFilePath = String.format("src/logs/%s_logs.xml", logTime);
		try {
			FileHandler handler = new FileHandler(logFilePath);
			logger.addHandler(handler);
			logger.setUseParentHandlers(false);
		} catch (IOException ex1) {
			logger.log(Level.SEVERE, null, ex1);
		}

		CasinoManager casino = new CasinoManager();
		casino.run();
	}
}